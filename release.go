package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

var (
	ConsoleReader = bufio.NewReader(os.Stdin)
)

func RunRelease() {
	var (
		stringAnswer string
		err          error
		branch       string
	)

	if len(ThisBuild.CommitString) < 1 {
		fmt.Printf("Enter commit text [default: empty]: ")
		stringAnswer, err = ConsoleReader.ReadString('\n')
		CheckError("RunRelease.1", err)

		ThisBuild.CommitString = strings.TrimSpace(stringAnswer)
	}

	InitText()

	if len(ThisBuild.Config.RSettings.PreCommand) > 0 {
		RunCommand(ThisBuild.Config.RSettings.PreCommand)
	}

	if ThisBuild.Config.RSettings.UseBranch {
		branch = TextProcessor.Replace(ThisBuild.Config.RSettings.BranchProto)
		RunCommand(fmt.Sprintf("git branch %s", branch))
		RunCommand(fmt.Sprintf("git checkout %s", branch))
	} else {
		branch = ThisBuild.Config.RSettings.StaticBranch
	}

	RunCommand(fmt.Sprintf("git add ."))
	commitText := TextProcessor.Replace(ThisBuild.Config.RSettings.CommitProto)
	RunCommand(fmt.Sprintf("git commit -m '%s'", commitText))

	if ThisBuild.Config.RSettings.AutoPush {
		RunCommand(fmt.Sprintf("git push origin %s", branch))
	}

	if len(ThisBuild.Config.RSettings.PostCommand) > 0 {
		RunCommand(ThisBuild.Config.RSettings.PostCommand)
	}
}
