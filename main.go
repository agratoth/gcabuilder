package main

import (
	"flag"
	"fmt"
	"os"
	"os/user"
	"time"
)

var (
	ThisBuild       Build
	ThisTime        = time.Now()
	ThisHostname    = MustString(os.Hostname())
	ThisUser, _     = user.Current()
	BuildFlag       = flag.Bool("build", true, "-build")
	ReleaseFlag     = flag.Bool("release", false, "-release")
	InitFlag        = flag.Bool("init", false, "-init")
	VersionFlag     = flag.Bool("v", false, "-v")
	StartThreadFlag = flag.Bool("startthread", false, "-startthread")
	ShowThreadFlag  = flag.Bool("showthread", false, "-showthread")
	CloseThreadFlag = flag.Bool("closethread", false, "-closethread")
	TempCommitText  = flag.String("commit", "", "-commit=<COMMITTEXT>")
)

func init() {
	flag.Parse()
	if !*VersionFlag {
		ThisBuild.Config = LoadBuildConfig("build.json")
	}
}

func main() {

	ThisBuild.CommitString = *TempCommitText

	if *VersionFlag {
		fmt.Printf("GCABuilder v.%s, builded %s\n", ThisBuildVersion, ThisBuildDate)
		os.Exit(0)
	}

	if *InitFlag {
		RunInit()
	} else if *ReleaseFlag {
		RunRelease()
	} else if *StartThreadFlag {
		StartThread()
	} else if *ShowThreadFlag {
		ShowThread()
	} else if *CloseThreadFlag {
		CloseThread()
	} else {
		RunBuild()
	}
}
