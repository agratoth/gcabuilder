package main

import (
	"fmt"
	"os"
	"os/exec"
	"strings"

	"github.com/mattn/go-shellwords"
)

type GCABCommand struct {
	Command string
	Args    []string
}

func (command *GCABCommand) Parse(source string) {
	parts, err := shellwords.Parse(TextProcessor.Replace(source))
	CheckError(fmt.Sprintf("Parse %s", source), err)

	for i, part := range parts {
		if i == 0 {
			command.Command = part
		} else {
			command.Args = append(command.Args, part)
		}
	}
}

func (command *GCABCommand) Join() string {
	return fmt.Sprintf("%s %s", command.Command, strings.Join(command.Args, " "))
}

func (command *GCABCommand) Run() {
	if len(command.Command) < 1 {
		return
	}
	execCmd := exec.Command(command.Command, command.Args...)
	execCmd.Stdout = os.Stdout
	CheckError(fmt.Sprintf("Run command '%s'", command.Join()), execCmd.Run())
}

func RunCommand(source string) {
	var (
		command GCABCommand
	)

	command.Parse(source)
	command.Run()
}
