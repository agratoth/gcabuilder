package main

import (
	"fmt"
	"strings"
)

func CheckThreads() (result bool) {
	if !ThisBuild.Config.RSettings.UseThread {
		fmt.Println("Треды отключены для текущего проекта. Измените значение release.usethread на true в build.json проекта")
	} else {
		result = true
	}
	return
}

func StartThread() {
	var (
		stringAnswer string
		err          error
	)

	if !CheckThreads() {
		return
	}

	if len(ThisBuild.Config.RSettings.CurrentThread) > 0 {
		fmt.Printf("В текущем проекте уже установлен тред [%s]. На данный момент поддерживается только один тред за раз.")
		return
	}

ENTER:
	fmt.Printf("Введите название треда: ")
	stringAnswer, err = ConsoleReader.ReadString('\n')
	CheckError("Thread name", err)

	stringAnswer = strings.TrimSpace(stringAnswer)

	if len(stringAnswer) > 0 {
		ThisBuild.Config.RSettings.CurrentThread = stringAnswer
		ThisBuild.Config.Save("build.json")
		fmt.Printf("Установлен новый тред [%s]\n", ThisBuild.Config.RSettings.CurrentThread)
	} else {
		fmt.Printf("Введено пустое значение. Ввести заново? [y/yes/n/no]: ")
		stringAnswer, err = ConsoleReader.ReadString('\n')
		CheckError("Repeat enter", err)
		stringAnswer = strings.TrimSpace(stringAnswer)
		if stringAnswer == "y" || stringAnswer == "yes" {
			goto ENTER
		}
	}
}

func ShowThread() {
	if !CheckThreads() {
		return
	}

	if len(ThisBuild.Config.RSettings.CurrentThread) > 0 {
		fmt.Printf("Установлен тред [%s]\n", ThisBuild.Config.RSettings.CurrentThread)
	} else {
		fmt.Println("Тред не установлен. Выполните gcabuilder -startthread")
	}
}

func CloseThread() {
	var (
		stringAnswer string
		err          error
	)

	if !CheckThreads() {
		return
	}

	if len(ThisBuild.Config.RSettings.CurrentThread) > 0 {
		fmt.Printf("Закрыть тред [%s]? [y/yes/n/no]: ", ThisBuild.Config.RSettings.CurrentThread)
		stringAnswer, err = ConsoleReader.ReadString('\n')
		CheckError("Close thread", err)
		stringAnswer = strings.TrimSpace(stringAnswer)
		if stringAnswer == "y" || stringAnswer == "yes" {
			fmt.Printf("Тред [%s] закрыт.\n", ThisBuild.Config.RSettings.CurrentThread)
			ThisBuild.Config.RSettings.CurrentThread = ""
			ThisBuild.Config.Save("build.json")
		}
	} else {
		fmt.Println("Тред не установлен. Закрывать нечего.")
	}
}
