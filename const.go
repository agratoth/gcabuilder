package main

const (
	//BuildTypeGobuild - Builder run command "go build"
	BuildTypeGobuild = "gobuild"

	//BuildTypeGoinstall - Builder run command "go install"
	BuildTypeGoinstall = "goinstall"

	//BuildTypeCustom - Builder run custom build command or script
	BuildTypeCustom = "custom"
)
