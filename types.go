package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

//Build - build struct
type Build struct {
	Config       BuildConfig
	CommitString string
}

//BuildConfig - build config struct
type BuildConfig struct {
	Info      AppInfo         `json:"info"`
	Version   BuildVersion    `json:"version"`
	BSettings BuildSettings   `json:"build"`
	RSettings ReleaseSettings `json:"release"`
}

type AppInfo struct {
	Name    string `json:"app-name"`
	Author  string `json:"app-author"`
	Company string `json:"app-company"`
}

//BuildVersion - build version struct
type BuildVersion struct {
	Major         int  `json:"major"`
	Minor         int  `json:"minor"`
	Build         int  `json:"build"`
	Autoincrement bool `json:"autoincrement"`
}

//BuildSettings - build settings struct
type BuildSettings struct {
	BuildPreCommand  string `json:"prebuild-command"`
	BuildProto       string `json:"proto"`
	BuildTarget      string `json:"target"`
	OutputParam      string `json:"output"`
	BuildType        string `json:"build-type"`
	BuildCommand     string `json:"build-command"`
	BuildPostCommand string `json:"postbuild-command"`
}

//ReleaseSettings - release settings struct
type ReleaseSettings struct {
	PreCommand    string `json:"prerelease-command"`
	UseThread     bool   `json:"usethread"`
	UseBranch     bool   `json:"usebranch"`
	AutoPush      bool   `json:"autopush"`
	CurrentThread string `json:"current-thread"`
	StaticBranch  string `json:"staticbranch"`
	BranchProto   string `json:"protobranch"`
	CommitProto   string `json:"protocommit"`
	PostCommand   string `json:"postrealese-command"`
}

//GetString return full version as string
func (version *BuildVersion) GetString() string {
	return fmt.Sprintf("%d.%d.%d",
		version.Major,
		version.Minor,
		version.Build)
}

//GetShortString - return short version as string
func (version *BuildVersion) GetShortString() string {
	return fmt.Sprintf("%d.%d",
		version.Major,
		version.Minor)
}

//LoadBuildConfig - load build config
func LoadBuildConfig(filename string) (result BuildConfig) {
	if *InitFlag {
		return
	}

	fileContent, err := ioutil.ReadFile(filename)
	CheckError("Read build config", err)

	err = json.Unmarshal(fileContent, &result)
	CheckError("Unmarshal build config", err)

	return
}

//Save - save build config
func (config *BuildConfig) Save(filename string) {
	fileContent, err := json.MarshalIndent(&config, "", "    ")
	CheckError("Marshal build config", err)

	err = ioutil.WriteFile(filename, fileContent, 0644)
	CheckError("Save build config", err)
}
