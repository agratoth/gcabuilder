package main

import (
	"fmt"
	"io/ioutil"
)

//RunBuild - run build operation
func RunBuild() {

	if ThisBuild.Config.Version.Autoincrement {
		ThisBuild.Config.Version.Build++
	}

	InitText()
	ThisBuild.Config.Save("build.json")

	protoFile, err := ioutil.ReadFile(ThisBuild.Config.BSettings.BuildProto)
	CheckError("Protofile read", err)

	replacedProtoFile := TextProcessor.Replace(string(protoFile))
	err = ioutil.WriteFile(ThisBuild.Config.BSettings.BuildTarget, []byte(replacedProtoFile), 0644)
	CheckError("Protofile fill", err)

	if len(ThisBuild.Config.BSettings.BuildPreCommand) > 0 {
		RunCommand(ThisBuild.Config.BSettings.BuildPreCommand)
	}

	switch ThisBuild.Config.BSettings.BuildType {
	case BuildTypeGobuild:
		if len(ThisBuild.Config.BSettings.OutputParam) > 0 {
			RunCommand(fmt.Sprintf("go build -o %s", ThisBuild.Config.BSettings.OutputParam))
		} else {
			RunCommand("go build")
		}
		break
	case BuildTypeGoinstall:
		RunCommand("go install")
		break
	case BuildTypeCustom:
		RunCommand(ThisBuild.Config.BSettings.BuildCommand)
		break
	}

	if len(ThisBuild.Config.BSettings.BuildPostCommand) > 0 {
		RunCommand(ThisBuild.Config.BSettings.BuildPostCommand)
	}
}
