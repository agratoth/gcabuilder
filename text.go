package main

import (
	"strings"
)

var (
	TextProcessor *strings.Replacer
)

//InitText - init text processor
func InitText() {
	TextProcessor = strings.NewReplacer(
		"{version}", ThisBuild.Config.Version.GetString(),
		"{shortversion}", ThisBuild.Config.Version.GetShortString(),
		"{date}", ThisTime.Format("02.01.2006"),
		"{time}", ThisTime.Format("15:04:05"),
		"{datetime}", ThisTime.Format("02.01.2006 15:04:05"),
		"{thread}", ThisBuild.Config.RSettings.CurrentThread,
		"{commit}", ThisBuild.CommitString,
		"{name}", ThisBuild.Config.Info.Name,
		"{author}", ThisBuild.Config.Info.Author,
		"{company}", ThisBuild.Config.Info.Company,
		"{hostname}", ThisHostname,
		"{user}", ThisUser.Name,
	)
}
