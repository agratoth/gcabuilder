# GCABuilder

Мета-сборщик приложений

## Установка

```bash
go get -u gitlab.com/agratoth/gcabuilder
```
Проверить установку можно командой
```bash
gcabuilder -v
# GCABuilder v.0.2.15, builded 11.11.2018
```
## Использование
- `gcabuilder -init` - инициализация проекта
- `gcabuilder -build` или `gcabuilder` - сборка проекта
- `gcabuilder -release -commit=<COMMITTEXT>` - релиз проекта
- `gcabuilder -startthread` - начать новый тред
- `gcabuilder -showthread` - показать текущий тред
- `gcabuilder -closethread` - закрыть текущий тред
- `gcabuilder -v` - версия сборщика

## Первоначальная настройка
Для инициализации пакета выполните команду (в корне проекта)
```bash
gcabuilder -init
```
GCABuilder создаст базовый файл build.json, в котором вы можете настроить логику работы сборщика. 
```json
{
    "info": {
        "app-name": "",
        "app-author": "",
        "app-company": ""
    },
    "version": {
        "major": 0,
        "minor": 0,
        "build": 0,
        "autoincrement": false
    },
    "build": {
	"prebuild-command": "",
        "proto": "",
        "target": "",
        "output": "",
        "build-type": "",
        "build-command": "",
        "postbuild-command": ""
    },
    "release": {
        "prerelease-command": "",
        "usethread": false,
        "usebranch": false,
        "autopush": false,
        "current-thread": "",
        "staticbranch": "",
        "protobranch": "",
        "protocommit": "",
        "postrealese-command": ""
    }
}
```
## Описание параметров build.json
* `info.app-name` - название приложения
* `info.app-author` - автор приложения
* `info.app-company` - компания
* `version.major` - мажорная версия проекта. Передвигается вручную
* `version.minor` - минорная версия проекта. Передвигается вручную
* `version.build` - номер билда. При `version.autoincrement = true` инкрементно возрастает при каждом запуске GCABuilder
* `version.autoincrement` - автоинкремент номера билда.
* `build.prebuild-command` - bash-команда или скрипт, запускаемый перед операцией сборки
* `build.proto` - имя файла-прототипа, заполняемого сборщиком. Прототипы используются, если собираемая программа должна содержать данные о версии своей сборки
* `build.target` - имя файла, формируемого сборщиком на основании `build.proto`
* `build.output` - используется для операций сборки `gobuild` и `goinstall`. Имя результирующего бинарного файла go-приложения
* `build.build-type` - тип операции сборки. Допустимые значения: 
    * `gobuild` - сборщик вызовет команду `go build`, 
    * `goinstall` - сборщик вызовет `go install`, 
    * `custom` - сборщик вызовет команду, указанную в `build.build-command`, 
    * `none` - сборщик не будет выполнять сборку
* `build.build-command` - кастомная bash-команда или скрипт для сборки проекта
* `build.postbuild-command` - bash-команда или скрипт, запускаемый после сборки
* `release.prerelease-command` - bash-команда или скрипт, запускается перед релизом
* `release.usethread` - при `true` использует метку трэда (можно использовать индекс issue или номер задачи)
* `release.usebranch` - при `true` создает при релизе новый бранч по шаблону из `release.protobranch`
* `release.autopush` - при `true` в конце релиза выполняет `git push origin <protobranch>` или `git push origin <staticbranch>`
* `release.current-thread` - текущий тред
* `release.staticbranch` - статический бранч
* `release.protobranch` - шаблон динамического бранча
* `release.protocommit` - шаблон сообщения при коммите
* `release.postrealese-command` - bash-команда или скрипт, запускается после релиза

## Маркеры
Маркеры можно использовать при составлении прототипов (шаблонов) коммитов (`release.protocommit`) и бранчей (`release.protocommit`), а так же во всех командах (prebuild, build, postbuild, prerelease, postrelease)
* `{version}` - строковое представление версии. Например, "1.4.5"
* `{shortversion}` - строковое представление краткой версии. Например, "1.4"
* `{date}` - текущая дата. Например, "21.11.2018"
* `{time}` - текущее время. Например, "21:06:13"
* `{datetime}` - текущие дата и время. Например, "21.11.2018 21:06:13"
* `{thread}` - тред, указанный при релизе. При `release.usethread = false` значение пустое
* `{commit}` - текст коммита
* `{name}` - значение из `info.app-name`
* `{author}` - значение из `info.app-author`
* `{company}` - значение из `info.app-company`
* `{hostname}` - системное значение Hostname
* `{user}` - имя текущего пользователя ОС


## Решение проблем
### gcabuilder: command not found
Решение:
* Добавить $GOPATH/bin в переменную окружения PATH:
```bash
$ export PATH=$PATH:$GOPATH/bin
```
* Или добавить симлинк на gcabuilder в `/usr/bin/`
```bash
$ sudo ln -s $GOPATH/bin/gcabuilder /usr/bin/gcabuilder
```


## Todos
- [x] Добавить в команды поддержку маркеров
- [x] Добавить маркеры `{hostname}`, `{user}`
- [ ] Добавить метакоммиты

## Лицензия CC BY-SA 3.0
Код данной программы предоставляется "как есть", доступен для любой модификации и любого использования, включая коммерческое. Пользуйтесь на здоровье. Фидбэк, как положительный, так и аргументированно отрицательный, приветствуется