package main

import (
	"fmt"
	"os"
)

//CheckError - check error object
func CheckError(place string, err error) {
	if err != nil {
		fmt.Printf("[%s ==> ERROR] %s\n", place, err.Error())
	}
}

func FileExists(filename string) bool {
	var result bool
	if _, err := os.Stat(filename); !os.IsNotExist(err) {
		result = true
	} else {
		result = false
	}
	return result
}

func MustString(value string, err error) (result string) {
	result = value
	CheckError("Must", err)
	return
}
